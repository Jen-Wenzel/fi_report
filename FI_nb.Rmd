---
title: "Forest inventory assignment"
output: html_notebook
---

Import data

```{r}
library(readr)
ref_cycle <- read_csv("data/a_group_1.csv")

all_trees <- read_csv("data/trees_group_1.csv")
head(all_trees)
```
Explore whole data

```{r}
#install.packages("tidyverse")
library(tidyverse)
dim(all_trees)
glimpse(all_trees)
summary(all_trees)
```

Show tree species frequency

```{r}
cycle_1 <- subset(all_trees, NR_CYKLU==1)
table(cycle_1$GAT)

# BK = Beech
# IWA = ?
# JD = White fir
# JRZ = Mountain ash
# JW = Sycamore maple
# MD = Larch
# OL = Black alder
# OS = ?
# SW = Norway spruce
# WB = Willow
```

Subset data to select species and take random sample

```{r}
beech <- subset(cycle_1, GAT=='BK')
beech <- beech[sample(1:nrow(beech), 120, replace=TRUE),]

fir <- subset(cycle_1, GAT=='JD')
fir <- fir[sample(1:nrow(fir), 120, replace=TRUE),]

spruce <- subset(cycle_1, GAT=='SW')
spruce <- spruce[sample(1:nrow(spruce), 120, replace=TRUE),]
```

Frequency of DBH classes

```{r}
hist(beech$d13_cm,
     breaks = 'Sturges',
     xlab= 'DBH in cm',
     main= 'Beech DBH',
     col = 4)
```

Get mean height and diameter per species

```{r}
library(rlang) 

mean_hd <- function(data, h, d){
  assign(paste0("mh_", deparse(substitute(data))), round(mean(data[[h]], na.rm = TRUE), digits=2), envir = .GlobalEnv)
  assign(paste0("md_", deparse(substitute(data))), round(mean(data[[d]], na.rm = TRUE), digits=2), envir = .GlobalEnv) 
}

mean_hd(beech, "H", "d13_cm")
mean_hd(fir, "H", "d13_cm")
mean_hd(spruce, "H", "d13_cm")

```

Set parameters for generalized height-diameter curves using the Näslund function

```{r}
o_beech <-0.6582
r_beech <- -0.4197

o_fir <-0.2566
r_fir <- -0.1420

o_spruce <-0.3341
r_spruce <- -0.2344

set_b <- function(s){
  assign(paste0("b_", deparse(substitute(s))), 
         get(paste0("o_", deparse(substitute(s)))) 
         *get(paste0("mh_", deparse(substitute(s))))**get(paste0("r_", deparse(substitute(s))))
         , envir = .GlobalEnv)
}

set_a <- function(s){
  assign(paste0("a_", deparse(substitute(s))), 
         get(paste0("md_", deparse(substitute(s))))/
         sqrt(get(paste0("mh_", deparse(substitute(s))))-1.3)
         -get(paste0("b_", deparse(substitute(s))))
         *get(paste0("md_", deparse(substitute(s)))), 
         envir = .GlobalEnv)
}

set_b(beech)
set_b(fir)
set_b(spruce)

set_a(beech)
set_a(fir)
set_a(spruce)
```

Calculate GHD

```{r}
beech$ghd <- (beech$d13_cm/(a_beech+b_beech*beech$d13_cm))**2+1.3
fir$ghd <- (fir$d13_cm/(a_fir+b_fir*fir$d13_cm))**2+1.3
spruce$ghd <- (spruce$d13_cm/(a_spruce+b_spruce*spruce$d13_cm))**2+1.3
```

Calculate merchantable wood form factor

```{r}
beech$f1 <- 0.46*md_beech**-0.008+(0.0059-0.0001*md_beech)*(md_beech-beech$d13_cm)
beech$s <- 1.1168-(48.115/beech$d13_cm**2)
beech$fq <- beech$f1*beech$s

fir$f1 <- 0.4132+0.4779/sqrt(mh_fir)+0.4426*mh_fir**-1.6259*(md_fir-fir$d13_cm)
fir$s <- 1-559.4519*fir$d13_cm**-3.5946
fir$fq <- fir$f1*fir$s

spruce$f1 <- 0.34+0.684/sqrt(spruce$d13_cm)
spruce$s <- 1-225.73*(spruce$d13_cm-1)**-3.2542
spruce$fq <- spruce$f1*spruce$s
```

Calculate over-bark merchantable wood volume

```{r}
beech$vol <- (pi/40000)*beech$d13_cm**2*beech$ghd*beech$fq
fir$vol <- (pi/40000)*fir$d13_cm**2*fir$ghd*fir$fq
spruce$vol <- (pi/40000)*spruce$d13_cm**2*spruce$ghd*spruce$fq
```

Volume distribution by species

```{r}
hist(beech$vol,
     breaks = 'Sturges',
     xlab= 'Vol in m3',
     main= 'Beech Volume',
     col = 4)

hist(fir$vol,
     breaks = 'Sturges',
     xlab= 'Vol in m3',
     main= 'Fir Volume',
     col = 4)

hist(spruce$vol,
     breaks = 'Sturges',
     xlab= 'Vol in m3',
     main= 'Spruce Volume',
     col = 4)
```

Real height vs estimated height

```{r}
library(forestmangr)
prmse_beech <- rmse_per(beech, "H", "ghd", na.rm = TRUE)
prmse_fir <- rmse_per(fir, "H", "ghd", na.rm = TRUE)
prmse_spruce <- rmse_per(spruce, "H", "ghd", na.rm = TRUE)

```

Calculate biomass by species

```{r}
#install.packages("remotes")
#remotes::install_github("ropensci/allodb")

library(allodb)

beech$bm <-
  get_biomass(
    dbh = beech$d13_cm,
    genus = "fagus",
    species = "sylvatica",
    coords = c(51.28, 21.02)
  )

fir$bm <-
  get_biomass(
    dbh = fir$d13_cm,
    genus = "abies",
    species = "concolor",
    coords = c(51.28, 21.02)
  )

spruce$bm <-
  get_biomass(
    dbh = spruce$d13_cm,
    genus = "picea",
    species = "abies",
    coords = c(51.28, 21.02)
  )

```

Calculate carbon content by species

```{r}
beech$cc <- beech$bm * 0.5
fir$cc <- fir$bm * 0.5
spruce$cc <- spruce$bm * 0.5
```

Write result to file

```{r}
write.csv(beech,"data/result_beech.csv", row.names = FALSE)
write.csv(fir,"data/result_fir.csv", row.names = FALSE)
write.csv(spruce,"data/result_spruce.csv", row.names = FALSE)
```






























